#include <signal.h>

#include <QDebug>
#include <QTimer>
#include <QMainWindow>
#include <DFApplication.hpp>

int main( int argc, char *argv[] ) {
    DFL::Application app( argc, argv );

    app.setApplicationName( "Signals Test" );
    app.setOrganizationName( "DFL" );
    app.setApplicationVersion( "0.0.1" );

    QObject::connect(
        &app, &DFL::Application::interrupted, [ = ] () {
            qDebug() << "Caught ^C. Continuing";
        }
    );

    QObject::connect(
        &app, &DFL::Application::terminate, [ = ] () {
            qDebug() << "Caught TERM/QUIT. Continuing";
        }
    );

    app.interceptSignal( SIGINT,  false );
    app.interceptSignal( SIGTERM, false );
    app.interceptSignal( SIGQUIT, false );
    app.interceptSignal( SIGSEGV, false );
    app.interceptSignal( SIGABRT, false );

    QMainWindow *win = new QMainWindow();

    win->show();

    QTimer::singleShot(
        1000,
        [ = ] () {
            qDebug() << "Sending SIGINT";
            kill( getpid(), SIGINT );
        }
    );

    QTimer::singleShot(
        2000,
        [ = ] () {
            qDebug() << "Sending SIGTERM";
            kill( getpid(), SIGTERM );
        }
    );

    QTimer::singleShot(
        3000,
        [ = ] () {
            qDebug() << "Sending SIGQUIT";
            kill( getpid(), SIGQUIT );
        }
    );

    QTimer::singleShot(
        4000,
        [ = ] () {
            qDebug() << "Sending SIGSEGV";
            kill( getpid(), SIGSEGV );
        }
    );

    return app.exec();
}
