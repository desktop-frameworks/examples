#include <unistd.h>
#include <signal.h>

#include <QDebug>
#include <DFCoreApplication.hpp>

int main( int argc, char *argv[] ) {
    DFL::CoreApplication *app = new DFL::CoreApplication( argc, argv );

    app->setApplicationName( "CoreApp Test" );
    app->setOrganizationName( "DFL" );

    qDebug() << "Locking app:" << app->lockApplication();

    if ( app->isRunning() ) {
        qDebug() << "Another instance found. Aborting...!";

        QStringList args = app->arguments();
        args.removeFirst();

        app->messageServer( args.join( " " ) );

        exit( 0 );
    }

    QObject::connect(
        app, &DFL::CoreApplication::messageFromClient, [ = ] ( QString msg, int ) {
            qDebug() << "Message received" << msg;

            if ( msg == "quit" ) {
                exit( 0 );
            }
        }
    );

    return app->exec();
}
