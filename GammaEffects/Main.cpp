#include <wayqt/WlGlobal.hpp>
#include <wayqt/Application.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/GammaControl.hpp>
#include <DFL/GammaEffects.hpp>
#include <QThread>

int main( int argc, char *argv[] ) {
    WQt::Application app( "Sunset", argc, argv );

    WQt::Registry            *reg      = app.waylandRegistry();
    WQt::GammaControlManager *gammaMgr = reg->gammaControlManager();

    wl_output         *output    = WQt::getWlOutput( app.primaryScreen() );
    WQt::GammaControl *gammaCtrl = gammaMgr->getGammaControl( output );

    DFL::GammaEffectsConfig cfg;

    cfg.sunrise = QTime( 6, 30, 0 );
    cfg.sunset  = QTime( 23, 30, 0 );
    cfg.gamma   = 1.5;

    DFL::GammaEffects *sunset = new DFL::GammaEffects( gammaCtrl );

    sunset->setConfiguration( cfg );

    // while ( cfg.brightness > 0.1 ) {
    //     cfg.brightness -= 0.05;
    //     sunset->setConfiguration( cfg );

    //     QThread::msleep( 100 );
    // }

    return app.exec();
}
