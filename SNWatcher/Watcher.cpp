/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * This example demonstrates how to write a simple status notifier
 * watcher daemon. Be sure to have DFL::SNI library installed
 **/

#include <QtCore>
#include <QDebug>
#include <QDBusConnectionInterface>

#include <dfl/SNWatcher.hpp>

int main( int argc, char *argv[] ) {
    QCoreApplication app( argc, argv );

    app.setOrganizationName( "DFL" );
    app.setApplicationName( "DFL Notfication Watcher" );
    app.setApplicationVersion( "1.0.0" );

    QCommandLineParser parser;

    parser.addHelpOption();         // Help
    parser.addVersionOption();      // Version

    parser.addOption( { { "d", "daemon" }, "Run this application as a daemon", "" } );
    parser.addOption( { "no-daemon", "Don't run this application as a daemon", "" } );

    parser.process( app );

    /*
     *
     * The reference used for writing this code into a daemon.
     * http://www.netzmafia.de/skripten/unix/linux-daemon-howto.html
     *
     */

    if ( not parser.isSet( "no-daemon" ) ) {
        pid_t pid = fork();

        if ( pid < 0 ) {
            // Exit the program
            exit( EXIT_FAILURE );
        }

        if ( pid > 0 ) {
            // Exit the parent
            exit( EXIT_SUCCESS );
        }

        /* Change the file mode mask */
        umask( 0 );

        /* Create a new SID for the child process */
        pid_t sid = setsid();

        if ( sid < 0 ) {
            // TODO: Write to log file
            exit( EXIT_FAILURE );
        }

        /* Change to home folder */
        if ( chdir( QDir::homePath().toUtf8().data() ) < 0 ) {
            // TODO: Write to error log
            exit( EXIT_FAILURE );
        }

        /* Close the std file descriptors */
        close( STDIN_FILENO );
        close( STDOUT_FILENO );
        close( STDERR_FILENO );
    }

    /* Our actual code starts here */

    StatusNotifierWatcher *sniw = new StatusNotifierWatcher();

    /* Some one else has taken this service!! Grrr... */
    if ( not sniw->isServiceRunning() ) {
        qDebug() << "Unable to start the service 'org.kde.StatusNotifierWatcher'.";
        qDebug() << "Please ensure that another instance of this app is not running elsewhere.";
        return EXIT_FAILURE;
    }

    /* Something went wrong... */
    else if ( not sniw->isObjectRegistered() ) {
        qDebug() << "Unable to register the object '/StatusNotifierWatcher'.";
        qDebug() << "Please ensure that another instance of this app is not running elsewhere.";
        return EXIT_FAILURE;
    }

    /* All is well. */
    else {
        qDebug() << "Service org.kde.StatusNotifierWatcher running...";
        qDebug() << "Object /StatusNotifierWatcher registered...";
        qDebug() << "Ready for incoming connections...";
    }

    return app.exec();
}
